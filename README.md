# Feedback for Cariboo Water Tool

To submit feedback:

* Goto the **[Issues section](https://bitbucket.org/foundryspatial/cwt-feedback/issues?status=new&status=open)**
* Click **[+ Create Issue](https://bitbucket.org/foundryspatial/cwt-feedback/issues/new)** button in the top right corner
* Provide a **Title** and **Description** for the feedback you'd like to provide and submit it by clicking the blue Create Issue button at the bottom of the form